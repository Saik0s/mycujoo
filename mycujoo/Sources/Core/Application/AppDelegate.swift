//
// AppDelegate.swift
//
// Created by Igor Tarasenko on 6/4/17
//

import UIKit

@UIApplicationMain
internal class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    lazy var app: ApplicationType = Application()

    func application(_: UIApplication, didFinishLaunchingWithOptions _: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        app.launch()
        return true
    }
}
