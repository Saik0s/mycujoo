//
// Application.swift
//
// Created by Igor Tarasenko.
//

import UIKit
import RxSwift
import RxCocoa
import AsyncDisplayKit

// TODO: Create Interface
public protocol ApplicationType {
    func launch()
}

public final class Application: ApplicationType {
    private let coordinatorFactory:   CoordinatorFactoryType
    private let navigationController: NavigationType
    private let bag:                  DisposeBag = DisposeBag()
    private let keyWindow:            UIWindow   = UIWindow(frame: UIScreen.main.bounds)
    private lazy var mainCoordinator: CoordinatorType = self.coordinatorFactory.makeCoordinator(for: .main)

    public init() {
#if RELEASE
        Logger.style = .default
        Logger.minLevel = .warning
        Logger.destinations = [.cutsom(closure: { _ in
            // CLSLogv("%@", getVaList([message]))
        })]
#elseif DEBUG
        Logger.style = .custom(options: [Logger.FormatOption](arrayLiteral: .date("HH:mm:ss.SSS"),
                                                              .space,
                                                              .file,
                                                              .string(":"),
                                                              .line,
                                                              .space,
                                                              .function,
                                                              .string(": "),
                                                              .message,
                                                              .wholeTextColorize))
#endif
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.window = keyWindow
        }

        navigationController = NavigationController(window: keyWindow)
        coordinatorFactory = CoordinatorFactory(navigation: navigationController)
    }

    public func launch() {
        Logger.info("Application did finish launching")

        mainCoordinator.start()
    }
}
