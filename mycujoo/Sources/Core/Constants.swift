//
// Constants.swift
//
// Created by Igor Tarasenko
//

import Foundation
import UIKit

public enum Constants {
    public static var defaultInset: UIEdgeInsets {
        return UIEdgeInsets(top: 10.0, left: 20.0, bottom: 20.0, right: 20.0)
    }
}
