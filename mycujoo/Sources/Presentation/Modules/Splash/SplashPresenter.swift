//
// SplashPresenter.swift
//
// Created by Igor Tarasenko
//

import Foundation
import RxSwift
import RxCocoa

public protocol SplashPresenterOutput {
}

public class SplashPresenter: SplashPresenterOutput {
    private let input:          SplashInteractorOutput
    private let bag:            DisposeBag           = DisposeBag()

    init(input: SplashInteractorOutput) {
        self.input = input
        bindInputs()
    }

    deinit {
        Logger.debug(#function)
    }

    func bindInputs() {
    }
}
