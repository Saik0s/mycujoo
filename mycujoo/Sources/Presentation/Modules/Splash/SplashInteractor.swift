//
// SplashInteractor.swift
//
// Created by Igor Tarasenko
//

import Foundation
import RxSwift
import RxCocoa

public protocol SplashInteractorOutput {
}

public protocol SplashInteractorDataStore {
}

public protocol SplashModuleOutput {
    var onDidLoad: PublishSubject<Void> { get }
}

public class SplashInteractor: InteractorType, SplashInteractorOutput, SplashInteractorDataStore, SplashModuleOutput {
    public let onDidLoad: PublishSubject<Void> = PublishSubject()

    public weak var input: SplashControllerOutput? {
        didSet {
            bindInput()
        }
    }

    private let bag: DisposeBag = DisposeBag()

    init() {
    }

    deinit {
        Logger.debug(#function)
    }

    func bindInput() {
        input?.didLoad.bind(to: onDidLoad).disposed(by: bag)
    }
}
