//
// SplashNode.swift
//
// Created by Igor Tarasenko
//

import Foundation
import AsyncDisplayKit
import RxSwift
import RxCocoa

public final class SplashNode: ASDisplayNode {
    // MARK: - Public properties
//    public var  validationResult: Variable<Bool>     = Variable(false)
//    public var  doneTap:          Driver<Void> {
//        return doneButton.rx.controlEvent(ASControlNodeEvent.touchUpInside)
//                            .throttle(0.3, latest: false, scheduler: MainScheduler.asyncInstance)
//                            .asDriver(onErrorJustReturn: ())
//    }
//    public var  addTap:           Driver<Void> {
//        return addButton.rx.controlEvent(ASControlNodeEvent.touchUpInside).asDriver()
//    }
//    public var  removeTap:        Driver<Void> {
//        return removeButton.rx.controlEvent(ASControlNodeEvent.touchUpInside).asDriver()
//    }
//    public var  saveTap:          Driver<Void> {
//        return saveButton.rx.controlEvent(ASControlNodeEvent.touchUpInside).asDriver()
//    }
//    public var  editTap:          Driver<Void> {
//        return editButton.rx.controlEvent(ASControlNodeEvent.touchUpInside).asDriver()
//    }
//    public var  nameValue:        ControlProperty<String?> {
//        return nameTextNode.rx.text // .asDriver(onErrorJustReturn: "").filterEmpty()
//    }
//    public var  titleValue:       ControlProperty<String?> {
//        return titleTextNode.rx.text // .asDriver(onErrorJustReturn: "").filterEmpty()
//    }
//    public var  username:         Driver<String> {
//        return usernameInput.rx.text.filterNil().asDriver(onErrorJustReturn: "").filterEmpty()
//    }
//    public var  password:         Driver<String> {
//        return passwordInput.rx.text.filterNil().asDriver(onErrorJustReturn: "").filterEmpty()
//    }
//    // MARK: - Private properties
//    private let bag:              DisposeBag         = DisposeBag()
    private let logoNode:         ASImageNode        = ASImageNode()
//    private let profileImageNode: ASImageNode        = ASImageNode()
//    private let backImageNode:    ASImageNode        = ASImageNode()
//    private let firstImageNode:   ASImageNode        = ASImageNode()
//    private let iconImageNode:    ASImageNode        = ASImageNode()
//    private let doneButton:       ASButtonNode       = ASButtonNode()
//    private let addButton:        ASButtonNode       = ASButtonNode()
//    private let removeButton:     ASButtonNode       = ASButtonNode()
//    private let saveButton:       ASButtonNode       = ASButtonNode()
//    private let editButton:       ASButtonNode       = ASButtonNode()
//    private let nameTextNode:     ASTextNode         = ASTextNode()
//    private let titleTextNode:    ASTextNode         = ASTextNode()
//    private let usernameInput:    ASEditableTextNode = ASEditableTextNode()
//    private let passwordInput:    ASEditableTextNode = ASEditableTextNode()

    // MARK: - Background thread
    public override init() {
        super.init()

        automaticallyManagesSubnodes = true
        setupNodes()
    }

    private func setupNodes() {
        backgroundColor = UIColor.red
        logoNode.backgroundColor = UIColor.blue
//        setStyle(Style.defaultContainer)
//        // shouldRasterizeDescendants = false
//        isLayerBacked = false
//        // logoNode.setStyle(Style.splashLogo)
//
//        doneButton.using(style: .done)
//        addButton.using(style: .login)
//        removeButton.using(style: .login)
//        saveButton.using(style: .login)
//        editButton.using(style: .login)
//
//        nameTextNode.attributedText = "FirstNotEditable".localized.using(style: .actionSheetLabel)
//        titleTextNode.attributedText = "SecondNotEditable".localized.using(style: .alertInput)
//        usernameInput.attributedText = "FirstEditable".localized.using(style: .cellInput)
//        passwordInput.attributedText = "SecondEditable".localized.using(style: .navigationSearchEntry)
//        passwordInput.isSecureTextEntry = true
//
//        logoNode.using(style: .splash)
//        profileImageNode.using(style: .splash)
//        backImageNode.using(style: .splash)
//        firstImageNode.using(style: .splash)
//        iconImageNode.using(style: .splash)

        // TODO: validationResult.asDriver().not().drive(doneButton.rx.isHidden).disposed(by: bag)
    }

    public override func layoutSpecThatFits(_: ASSizeRange) -> ASLayoutSpec {
        //        // Layout element
        //        node.style.width
        //        node.style.height
        //        node.style.maxSize
        //        node.style.minSize
        //        node.style.maxWidth
        //        node.style.minWidth
        //        node.style.maxHeight
        //        node.style.minHeight
        //        node.style.maxLayoutSize
        //        node.style.minLayoutSize
        //        node.style.preferredSize
        //        node.style.preferredLayoutSize

        //        // Stack Layout element
        //        node.style.flexGrow
        //        node.style.ascender
        //        node.style.flexBasis
        //        node.style.alignSelf
        //        node.style.descender
        //        node.style.flexShrink
        //        node.style.spacingAfter
        //        node.style.spacingBefore

        //        // dimension returned is relative (%)
        //        ASDimensionMake("50%")
        //        ASDimensionMakeWithFraction(0.5)
        //
        //        // dimension returned in points
        //        ASDimensionMake("70pt")
        //        ASDimensionMake(70)
        //        ASDimensionMakeWithPoints(70)
        //        ASLayoutSizeMake(_ width: ASDimension, _ height: ASDimension)
        //        // Dimension type "Auto" indicates that the layout element may
        //        // be resolved in whatever way makes most sense given the circumstances
        //        let width = ASDimensionMake(.auto, 0)
        //        let height = ASDimensionMake("50%")
        //
        //        layoutElement.style.preferredLayoutSize = ASLayoutSizeMake(width, height)

        //        // ASStackLayoutSpec
        //        ASStackLayoutSpec.vertical()
        //        ASStackLayoutSpec.horizontal()
        //        stack.direction
        //        stack.spacing               The amount of space between each child.
        //        stack.horizontalAlignment   Specifies how children are aligned horizontally.
        //                                    Depends on the stack direction
        //        stack.verticalAlignment     Specifies how children are aligned vertically.
        //                                    Depends on the stack direction
        //        stack.justifyContent        The amount of space between each child.
        //        stack.alignItems            Orientation of children along cross axis

        logoNode.style.preferredSize = CGSize(width: 50, height: 50)
                return ASCenterLayoutSpec(horizontalPosition: .center,
                                          verticalPosition: .center,
                                          sizingOption: [],
                                          child: logoNode)

//        let rightStack: ASStackLayoutSpec = ASStackLayoutSpec.vertical()
//        rightStack.children = [doneButton, addButton, removeButton, saveButton, editButton]
//        let leftStack: ASStackLayoutSpec = ASStackLayoutSpec.vertical()
//        leftStack.children = [nameTextNode, titleTextNode, usernameInput, passwordInput]
//        leftStack.style.flexBasis = ASDimensionMake("50%")
//        rightStack.style.flexBasis = ASDimensionMake("50%")
//        leftStack.children?.forEach { (child: ASLayoutElement) in
//            child.style.flexGrow = 1.0
//        }
//        rightStack.children?.forEach { (child: ASLayoutElement) in
//            child.style.flexGrow = 1.0
//        }
//        let bottomStack: ASStackLayoutSpec = ASStackLayoutSpec.horizontal()
//        bottomStack.children = [leftStack, rightStack]
//        let topStack: ASStackLayoutSpec = ASStackLayoutSpec.horizontal()
//        topStack.children = [logoNode, profileImageNode, backImageNode, firstImageNode, iconImageNode]
//        topStack.style.flexBasis = ASDimensionMake("30%")
//        topStack.children?.forEach { (child: ASLayoutElement) in
//            child.style.flexShrink = 1.0
//        }
//        bottomStack.style.flexBasis = ASDimensionMake("70%")
//        let mainStack: ASStackLayoutSpec = ASStackLayoutSpec.vertical()
//        mainStack.children = [topStack, bottomStack]
//        mainStack.style.flexGrow = 1.0
//        return mainStack
    }

    public override func animateLayoutTransition(_ context: ASContextTransitioning) {
        super.animateLayoutTransition(context)
    }

    // MARK: - Main thread
    public override func didLoad() {
        super.didLoad()
    }

    public override func layout() {
        super.layout()
    }
}
