//
// SplashController.swift
//
// Created by Igor Tarasenko
//

import Foundation
import AsyncDisplayKit
import RxSwift
import RxCocoa

public protocol SplashControllerOutput: class {
    var didLoad: PublishSubject<Void> { get }
}

public class SplashController: ASViewController<SplashNode>, SplashControllerOutput {
    public let didLoad: PublishSubject<Void> = PublishSubject()

    private let input: SplashPresenterOutput
    private let bag:   DisposeBag = DisposeBag()

    init(input: SplashPresenterOutput) {
        self.input = input
        super.init(node: SplashNode())

        bindInput()
    }

    public required init?(coder _: NSCoder) {
        fatalError("Not Implemented")
    }

    deinit {
        Logger.debug(#function)
    }

    func bindInput() {
    }

    public override func viewDidLoad() {
        super.viewDidLoad()

        didLoad.onNext(())
    }

    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    public override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }

    public override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        coordinator.animate(alongsideTransition: { _ in
            self.node.transitionLayout(with: ASSizeRange(min: size, max: size),
                                       animated: true,
                                       shouldMeasureAsync: true)
        }, completion: nil)
    }
}
