//
// SplashModuleFactory.swift
//
// Created by Igor Tarasenko
//

import Foundation

public protocol SplashModuleFactoryType: class {
    func makeSplashModule() -> (SplashModuleOutput, Presentable)
}

extension SplashModuleFactoryType where Self: ModuleFactoryType {
    public func makeSplashModule() -> (SplashModuleOutput, Presentable) {
        let interactor: SplashInteractor = SplashInteractor()
        let presenter:  SplashPresenter  = SplashPresenter(input: interactor)
        let controller: SplashController = SplashController(input: presenter).then {
            interactor.input = $0
        }

        return (interactor, controller)
    }
}
