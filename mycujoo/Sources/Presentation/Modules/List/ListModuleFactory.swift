//
// ListModuleFactory.swift
//
// Created by Igor Tarasenko
//

import Foundation

public protocol ListModuleFactoryType: class {
    func makeListModule() -> (ListModuleOutput, Presentable)
}

extension ListModuleFactoryType where Self: ModuleFactoryType {
    public func makeListModule() -> (ListModuleOutput, Presentable) {
        let interactor: ListInteractor = ListInteractor()
        let presenter:  ListPresenter  = ListPresenter(input: interactor)
        let controller: ListController = ListController(input: presenter).then {
            interactor.input = $0
        }

        return (interactor, controller)
    }
}
