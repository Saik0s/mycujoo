//
// ListModels.swift
//
// Created by Igor Tarasenko
//

import Foundation

public enum List {
    public enum Update {
        public struct Request {
        }

        public struct Response {
        }

        public struct ViewModel {
        }
    }

    public enum Submit {
        public struct Request {
        }

        public struct Response {
        }

        public struct ViewModel {
        }
    }

    public enum Edit {
        public struct Request {
        }

        public struct Response {
        }

        public struct ViewModel {
        }
    }

    public enum Delete {
        public struct Request {
        }

        public struct Response {
        }

        public struct ViewModel {
        }
    }
}
