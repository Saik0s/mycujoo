//
// ListController.swift
//
// Created by Igor Tarasenko
//

import Foundation
import AsyncDisplayKit
import RxSwift
import RxCocoa

public protocol ListControllerOutput: class {
}

public class ListController: ASViewController<ListNode>, ListControllerOutput {
    private let input: ListPresenterOutput
    private let bag:   DisposeBag = DisposeBag()

    init(input: ListPresenterOutput) {
        self.input = input
        super.init(node: ListNode())

        bindInput()
    }

    public required init?(coder _: NSCoder) {
        fatalError("Not Implemented")
    }

    deinit {
        Logger.debug(#function)
    }

    func bindInput() {
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
    }

    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    public override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }

    public override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        coordinator.animate(alongsideTransition: { _ in
            self.node.transitionLayout(with: ASSizeRange(min: size, max: size),
                                       animated: true,
                                       shouldMeasureAsync: true)
        }, completion: nil)
    }
}
