//
// ListPresenter.swift
//
// Created by Igor Tarasenko
//

import Foundation
import RxSwift
import RxCocoa

public protocol ListPresenterOutput {
}

public class ListPresenter: ListPresenterOutput {
    private let input:          ListInteractorOutput
    private let bag:            DisposeBag           = DisposeBag()

    init(input: ListInteractorOutput) {
        self.input = input

        bindInputs()
    }

    deinit {
        Logger.debug(#function)
    }

    func bindInputs() {
    }
}
