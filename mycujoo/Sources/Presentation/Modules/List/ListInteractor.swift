//
// ListInteractor.swift
//
// Created by Igor Tarasenko
//

import Foundation
import RxSwift
import RxCocoa

public protocol ListInteractorOutput {
}

public protocol ListInteractorDataStore {
}

public protocol ListModuleOutput {
    var onDidSelect: PublishSubject<UInt> { get }
}

public class ListInteractor: InteractorType, ListInteractorOutput, ListInteractorDataStore, ListModuleOutput {
    public private(set) var onDidSelect: PublishSubject<UInt> = PublishSubject()

    public weak var  input: ListControllerOutput? {
        didSet {
            bindInput()
        }
    }

    private let bag:   DisposeBag = DisposeBag()

    init() {
    }

    deinit {
        Logger.debug(#function)
    }

    func bindInput() {
    }
}
