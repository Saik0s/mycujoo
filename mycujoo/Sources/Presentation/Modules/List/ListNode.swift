//
// ListNode.swift
//
// Created by Igor Tarasenko
//

import Foundation
import AsyncDisplayKit
import RxSwift
import RxCocoa

public final class ListNode: ASDisplayNode {
    // MARK: - Public properties

    // MARK: - Private properties
    private let bag: DisposeBag = DisposeBag()

    // MARK: - Background thread
    public override init() {
        super.init()

        automaticallyManagesSubnodes = true
        setupNodes()
    }

    private func setupNodes() {
        set(style: .defaultContainer)
    }

    public override func layoutSpecThatFits(_: ASSizeRange) -> ASLayoutSpec {
        return ASLayoutSpec()
    }

    public override func animateLayoutTransition(_ context: ASContextTransitioning) {
        super.animateLayoutTransition(context)
    }

    // MARK: - Main thread
    public override func didLoad() {
        super.didLoad()
    }

    public override func layout() {
        super.layout()
    }
}
