//
// Styles.swift
//
// Created by Igor Tarasenko
//

import UIKit
import AsyncDisplayKit

public enum Style<Node> {
    public typealias StyleClosure = (Node) -> Node

    case container(StyleClosure)

    public var closure: StyleClosure {
        switch self {
            case let .container(styleClosure):
                return styleClosure
        }
    }
}

public typealias Font = UIFont
public typealias Color = UIColor

extension UIFont {
    public static let main14: UIFont = /* R.font.openSans(size: 14) ?? */ UIFont.systemFont(ofSize: 14)
    public static let main16: UIFont = /* R.font.openSans(size: 16) ?? */ UIFont.systemFont(ofSize: 16)
    public static let main18: UIFont = /* R.font.openSans(size: 18) ?? */ UIFont.systemFont(ofSize: 18)
    public static let main20: UIFont = /* R.font.openSans(size: 20) ?? */ UIFont.systemFont(ofSize: 20)
    public static let main22: UIFont = /* R.font.openSans(size: 22) ?? */ UIFont.systemFont(ofSize: 22)
    public static let main24: UIFont = /* R.font.openSans(size: 24) ?? */ UIFont.systemFont(ofSize: 24)

    public static let mainBold14: UIFont = /* R.font.openSansBold(size: 14) ?? */ UIFont.systemFont(ofSize: 14)
    public static let mainBold16: UIFont = /* R.font.openSansBold(size: 16) ?? */ UIFont.systemFont(ofSize: 16)
    public static let mainBold18: UIFont = /* R.font.openSansBold(size: 18) ?? */ UIFont.systemFont(ofSize: 18)
    public static let mainBold20: UIFont = /* R.font.openSansBold(size: 20) ?? */ UIFont.systemFont(ofSize: 20)
    public static let mainBold22: UIFont = /* R.font.openSansBold(size: 22) ?? */ UIFont.systemFont(ofSize: 22)
    public static let mainBold24: UIFont = /* R.font.openSansBold(size: 24) ?? */ UIFont.systemFont(ofSize: 24)

    public static let mainSemibold14: UIFont = /* R.font.openSansSemibold(size: 14) ?? */ UIFont.systemFont(ofSize: 14)
    public static let mainSemibold16: UIFont = /* R.font.openSansSemibold(size: 16) ?? */ UIFont.systemFont(ofSize: 16)
    public static let mainSemibold18: UIFont = /* R.font.openSansSemibold(size: 18) ?? */ UIFont.systemFont(ofSize: 18)
    public static let mainSemibold20: UIFont = /* R.font.openSansSemibold(size: 20) ?? */ UIFont.systemFont(ofSize: 20)
    public static let mainSemibold22: UIFont = /* R.font.openSansSemibold(size: 22) ?? */ UIFont.systemFont(ofSize: 22)
    public static let mainSemibold24: UIFont = /* R.font.openSansSemibold(size: 24) ?? */ UIFont.systemFont(ofSize: 24)
}

extension UIColor {
}
