//
// NodeStyles.swift
//
// Created by Igor Tarasenko
//

import AsyncDisplayKit

extension ASDisplayNode {
    @discardableResult
    public func set(style: Style<ASDisplayNode>) -> ASDisplayNode {
        return style.closure(self)
    }
}

extension Style where Node == ASDisplayNode {
    public static var defaultContainer: Style {
        return .container { (node: ASDisplayNode) -> ASDisplayNode in
            node.backgroundColor = .green
            return node
        }
    }
}
