//
// ImageStyles.swift
//
// Created by Igor Tarasenko
//

import UIKit
import AsyncDisplayKit

extension ASImageNode {
    @discardableResult
    public func style(as imageStyle: Style<ASImageNode>) -> ASImageNode {
        return imageStyle.closure(self)
    }
}

extension Style where Node == ASImageNode {
    public static var test: Style {
        return .container { imageNode in
            imageNode
        }
    }
}
