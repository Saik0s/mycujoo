//
// ButtonStyles.swift
//
// Created by Igor Tarasenko
//

import UIKit
import AsyncDisplayKit

extension ASButtonNode {
    @discardableResult
    public func style(as buttonStyle: Style<ASButtonNode>) -> ASButtonNode {
        return buttonStyle.closure(self)
    }
}

extension Style where Node == ASButtonNode {
    public static var test: Style {
        return .container { button in
            button
        }
    }
}
