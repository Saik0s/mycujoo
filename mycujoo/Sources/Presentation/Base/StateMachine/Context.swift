//
// Context.swift
//
// Created by Igor Tarasenko
//

import Foundation
import RxSwift
import RxCocoa
import RxOptional

public protocol ContextTypeDependency {
    associatedtype State: StateType
    associatedtype Input where Input == State.Input
    associatedtype InputClosure
    associatedtype StateClosure
}

public protocol ContextInterface: ContextTypeDependency {
    var currentState: Property<State> { get }
    var newInput:     AnyObserver<Input> { get }

    init(inputHandler: InputClosure, stateHandler: StateClosure)

    func pop() -> State
}

private protocol ContextType: ContextTypeDependency {
    var state:           Variable<State> { get }
    var stateStack:      Stack<State> { get }
    var newInputSubject: PublishSubject<Input> { get }
    var inputHandler:    InputClosure { get }
    var stateHandler:    StateClosure { get }
}

public final class Context<S:StateType, I>: ContextType where I == S.Input {
    public let currentState: Property<S>
    public var newInput:     AnyObserver<I> {
        return newInputSubject.asObserver()
    }

    private let     bag:             DisposeBag        = DisposeBag()

    fileprivate var state:           Variable<S>       = Variable(S.initial)
    fileprivate var stateStack:      Stack<S>          = Stack(initial: S.initial)
    fileprivate var newInputSubject: PublishSubject<I> = PublishSubject()
    fileprivate var inputHandler:    InputClosure
    fileprivate var stateHandler:    StateClosure

    public init(inputHandler: @escaping InputClosure, stateHandler: @escaping StateClosure) {
        self.inputHandler = inputHandler
        self.stateHandler = stateHandler
        currentState = Property(capturing: state)
        newInputSubject.asObservable()
                       .withLatestFrom(state.asObservable(), resultSelector: inputHandler)
                       .filterNil()
                       .bind(onNext: push(newState:))
                       .disposed(by: bag)
    }
}

extension Context: ContextInterface {
    public typealias State = S
    public typealias Input = I
    public typealias InputClosure = (_ input: I, _ state: S) -> S?
    public typealias StateClosure = (_ state: S) -> Void

    public func pop() -> State {
        fatalError("pop() has not been implemented")
    }

    private func push(newState: S) {
        stateStack.push(newState)
        state.value = newState
        stateHandler(newState)
    }
}
