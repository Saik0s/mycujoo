//
// Stack.swift
//
// Created by Igor Tarasenko on 28/09/17.
//

import Foundation

public struct Stack<Element> {
    private var array: [Element] = []

    public init(initial: Element) {
        array = [initial]
    }

    public var  count: Int {
        return array.count
    }

    public mutating func push(_ element: Element) {
        array.append(element)
    }

    public mutating func pop() -> Element? {
        return array.popLast()
    }

    public var top: Element? {
        return array.last
    }
}

extension Stack where Element: Equatable {
    public func contains(_ element: Element) -> Bool {
        return array.contains {
            $0 == element
        }
    }
}
