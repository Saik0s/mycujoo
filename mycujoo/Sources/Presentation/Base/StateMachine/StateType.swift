//
// Created by Igor Tarasenko on 28/09/17.
//

import Foundation
import RxSwift
import RxCocoa

public protocol StateType {
    associatedtype Input

    static var initial: Self { get }
}
