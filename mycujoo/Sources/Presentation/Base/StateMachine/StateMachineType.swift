//
// StateMachineType.swift
//
// Created by Igor Tarasenko
//

import Foundation
import RxSwift

public protocol StateMachineType: class {
    associatedtype State: StateType

    var context: Context<State, State.Input> { get }
}
