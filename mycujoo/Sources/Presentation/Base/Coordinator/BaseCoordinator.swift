//
// BaseCoordinator.swift
//
// Created by Igor Tarasenko on 07/10/17.
//

import Foundation
import RxSwift

public protocol BaseCoordinatorType {
    associatedtype ChildModuleFactoryType

    var navigation:    NavigationType { get }
    var moduleFactory: ChildModuleFactoryType { get }

    init(navigation: NavigationType, moduleFactory: ChildModuleFactoryType)
}

public class BaseCoordinator<ModuleF>: BaseCoordinatorType {
    public let navigation:    NavigationType
    public let moduleFactory: ModuleF

    let bag: DisposeBag = DisposeBag()

    public required init(navigation: NavigationType, moduleFactory: ModuleF) {
        self.navigation = navigation
        self.moduleFactory = moduleFactory
    }
}
