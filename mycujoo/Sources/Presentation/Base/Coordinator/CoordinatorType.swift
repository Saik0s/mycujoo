//
// CoordinatorType.swift
//
// Created by Igor Tarasenko
//

import Foundation
import RxSwift

public protocol CoordinatorType {
    func start()
}
