//
// Presentable.swift
//
// Created by Igor Tarasenko on 07/10/17.
//

import UIKit

public protocol Presentable {
    // TODO: equatable using view controllers
    var viewController: UIViewController { get }
}

extension UIViewController: Presentable {
    public var viewController: UIViewController {
        return self
    }
}
