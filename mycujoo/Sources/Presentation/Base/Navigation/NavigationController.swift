//
// Created by Igor Tarasenko on 24/09/17.
//

import Foundation
import AsyncDisplayKit
import RxSwift
import RxCocoa
import RxSwiftExt
import RxOptional

public class NavigationController: NavigationType {
    public var didPop: PublishSubject<Void> = PublishSubject()

    private let controller: ASNavigationController
    private let window:     UIWindow
    private let bag:        DisposeBag = DisposeBag()

    private var previous: Variable<[UIViewController]> = Variable([])

    init(window: UIWindow,
         controller: ASNavigationController = ASNavigationController(navigationBarClass: nil, toolbarClass: nil)) {
        self.window = window
        self.controller = controller
        window.rootViewController = controller
        window.makeKeyAndVisible()

        controller.rx.didShow.bind { _ in
            self.previous.value = self.controller.viewControllers
        }.disposed(by: bag)

        let last: Observable<Presentable?> = previous.asObservable().map {
            $0.count >= 2 ? $0.reversed()[1] as? Presentable : nil
        }

        controller.rx.willShow.map {
            $0.viewController
        }.withLatestFrom(last) { (next: UIViewController, previous: Presentable?) -> Bool in
            if let previous = previous?.viewController {
                return next == previous
            }
            return false
        }.filter {
            $0
        }.mapTo(()).bind(to: didPop).disposed(by: bag)
    }

    public func perform(_ transition: Transition) -> Presentable {
        switch transition {
            case let .root(presentable):
                controller.setViewControllers([presentable.viewController], animated: true)
                return presentable
            case let .push(presentable):
                controller.pushViewController(presentable.viewController, animated: true)
                return presentable
            case let .modal(presentable):
                controller.present(presentable.viewController, animated: true)
                return presentable
            case .pop:
                if let presentable: Presentable = controller.popViewController(animated: true) {
                    return presentable
                } else {
                    preconditionFailure()
                }
        }
    }

    public func performing(_ transition: Transition) -> Observable<Presentable> {
        return Observable.just(perform(transition))
    }
}
