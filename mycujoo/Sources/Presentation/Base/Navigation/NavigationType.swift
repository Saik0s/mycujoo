//
// NavigationType.swift
// mycujoo
//
// Created by Igor Tarasenko
//

import Foundation
import RxSwift
import RxCocoa

public enum Transition {
    case root(Presentable)
    case push(Presentable)
    case modal(Presentable)
    case pop
}

public protocol NavigationType {
    var didPop: PublishSubject<Void> { get }

    @discardableResult
    func perform(_ transition: Transition) -> Presentable
    @discardableResult
    func performing(_ transition: Transition) -> Observable<Presentable>
}
