//
// Created by Igor Tarasenko on 24/09/17.
//

import Foundation

public enum CoordinatorName {
    case main
}

public protocol CoordinatorFactoryType {
    func makeCoordinator(for name: CoordinatorName) -> CoordinatorType
}

public class CoordinatorFactory: CoordinatorFactoryType {
    let navigation: NavigationType
    let moduleFactory: ModuleFactory = ModuleFactory()

    init(navigation: NavigationType) {
        self.navigation = navigation
    }

    public func makeCoordinator(for name: CoordinatorName) -> CoordinatorType {
        switch name {
            case .main:
                return MainCoordinator(navigation: navigation, moduleFactory: moduleFactory)
        }
    }
}
