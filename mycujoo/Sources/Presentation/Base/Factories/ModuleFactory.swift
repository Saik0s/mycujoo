//
// Created by Igor Tarasenko on 24/09/17.
//

import Foundation

public protocol ModuleFactoryType: class {
}

public class ModuleFactory: ModuleFactoryType {

    init() {
    }
}

extension ModuleFactory: SplashModuleFactoryType {
}

extension ModuleFactory: ListModuleFactoryType {
}
