//
// MainCoordinator.swift
//
// Created by Igor Tarasenko
//

import Foundation
import RxSwift
import RxCocoa
import RxSwiftExt
import RxOptional

public final class MainCoordinator: BaseCoordinator<SplashModuleFactoryType & ListModuleFactoryType> {
    public enum State: StateType {
        case launching
        case videoList
        case halfPlayer(link: String)
        case fullPlayer(link: String, progress: Double, highlights: [Double])

        public enum Input {
            case didLaunch
            case didSelectVideo(id: String)
        }

        public static let initial: State = .launching
    }

    public lazy var context: Context<State, State.Input> = {
        Context<State, State.Input>(inputHandler: { input, state in
            switch (input, state) {
                case (.didLaunch, .launching):
                    return .videoList
                case let (.didSelectVideo(id), .videoList):
                    return .halfPlayer(link: id)
                default:
                    return nil
            }
        }, stateHandler: { state in
            switch state {
                case .launching: self.presentSplashScreen()
                case .videoList: self.presentListScreen()
                case .halfPlayer(link: _): self.pushHalfPlayerScreen()
                case .fullPlayer(link: _, progress: _, highlights: _): self.pushFullPlayerScreen()
            }
        })
    }()

    deinit {
        Logger.debug(#function)
    }
}

extension MainCoordinator: CoordinatorType {
    public func start() {
        navigation.didPop.bind { _ in
            Logger.error("Back pressed")
        }.disposed(by: bag)
    }

    func presentSplashScreen() {
        let (output, viewController) = moduleFactory.makeSplashModule()
        navigation.perform(.push(viewController))
        output.onDidLoad.mapTo(State.Input.didLaunch).bind(to: context.newInput).disposed(by: bag)
    }

    func presentListScreen() {
        let (output, viewController) = moduleFactory.makeListModule()
        navigation.perform(.push(viewController))
        output.onDidSelect.map { id in
            State.Input.didSelectVideo(id: "\(id)")
        }.bind(to: context.newInput).disposed(by: bag)
    }

    func pushHalfPlayerScreen() {
    }

    func pushFullPlayerScreen() {
    }
}

extension MainCoordinator: StateMachineType {
}