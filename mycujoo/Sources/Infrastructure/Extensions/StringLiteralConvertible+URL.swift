//
// StringLiteralConvertible+URL.swift
// Switchback
//
// Created by Igor Tarasenko
// Copyright (c) 2017 Igor Tarasenko. All rights reserved.
//

import Foundation

extension ExpressibleByStringLiteral where StringLiteralType == String {
    public init(extendedGraphemeClusterLiteral value: String) {
        self.init(stringLiteral: value)
    }

    public init(unicodeScalarLiteral value: String) {
        self.init(stringLiteral: value)
    }
}

// extension URL: ExpressibleByStringLiteral {
//    public init(stringLiteral value: String) {
//        self = URL(string: value).require()
//    }
// }

extension String {
    public func toURL() -> URL? {
        return URL(string: self)
    }

    public func toFileURL() -> URL? {
        return URL(fileURLWithPath: self)
    }
}
