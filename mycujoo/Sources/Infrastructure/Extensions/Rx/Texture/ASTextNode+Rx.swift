//
// ASTextNode+Rx.swift
// Switchback
//
// Created by Igor Tarasenko
// Copyright (c) 2017 . All rights reserved.
//

import Foundation
import UIKit
import AsyncDisplayKit
import RxSwift
import RxCocoa
import RxOptional

extension Reactive where Base: ASTextNode {

    public var text: ControlProperty<String?> {
        return value
    }

    public var value: ControlProperty<String?> {
        return ASControlNode.rx.value(base, getter: { (textNode: ASTextNode) in
            textNode.attributedText?.string
        }, setter: { (textNode: ASTextNode, value: String?) in
            if textNode.attributedText?.string != value {
                guard let attrString = textNode.attributedText else {
                    textNode.attributedText = NSAttributedString(string: value ?? "")
                    return
                }
                let mutable: NSMutableAttributedString = NSMutableAttributedString(attributedString: attrString)
                mutable.mutableString.setString(value ?? "")
                textNode.attributedText = mutable
            }
        })
    }
}
