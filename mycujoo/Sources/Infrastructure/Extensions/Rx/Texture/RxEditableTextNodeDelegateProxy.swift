//
// RxEditableTextNodeDelegateProxy.swift
// Switchback
//
// Created by Igor Tarasenko
// Copyright (c) 2017 . All rights reserved.
//

import UIKit
import AsyncDisplayKit
import RxSwift
import RxCocoa

public class RxEditableTextNodeDelegateProxy: DelegateProxy, UITextViewDelegate, DelegateProxyType {

    /// For more information take a look at `DelegateProxyType`.
    public override class func createProxyForObject(_ object: AnyObject) -> AnyObject {

        guard let textNode: ASEditableTextNode = object as? ASEditableTextNode else {
            fatalError("createProxyForObject")
        }
        return textNode.textView.createRxDelegateProxy()
    }

    /// For more information take a look at `DelegateProxyType`.
    public class func setCurrentDelegate(_ delegate: AnyObject?, toObject object: AnyObject) {

        guard let textNode: ASEditableTextNode = object as? ASEditableTextNode,
              let delegate = delegate as? ASEditableTextNodeDelegate else {
            fatalError("setCurrentDelegate")
        }

        textNode.delegate = delegate
    }

    /// For more information take a look at `DelegateProxyType`.
    public class func currentDelegateFor(_ object: AnyObject) -> AnyObject? {

        guard let textNode: ASEditableTextNode = object as? ASEditableTextNode else {
            fatalError("currentDelegateFor")
        }
        return textNode.delegate
    }

    public private(set) weak var textView: UITextView?

    public required init(parentObject: AnyObject) {

        guard let textNode: ASEditableTextNode = parentObject as? ASEditableTextNode else {
            fatalError("init")
        }
        super.init(parentObject: textNode.textView)
    }

    @objc
    public func textView(_ textView: UITextView,
                         shouldChangeTextIn range: NSRange,
                         replacementText text: String) -> Bool {

        let forwardToDelegate = self.forwardToDelegate() as? UITextViewDelegate
        return forwardToDelegate?.textView?(textView, shouldChangeTextIn: range, replacementText: text) ?? true
    }
}
