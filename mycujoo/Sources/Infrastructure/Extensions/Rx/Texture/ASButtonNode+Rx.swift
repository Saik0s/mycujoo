//
// ASButtonNode+Rx.swift
// Switchback
//
// Created by Igor Tarasenko
// Copyright (c) 2017 Igor Tarasenko. All rights reserved.
//

import Foundation
import UIKit
import AsyncDisplayKit
import RxSwift
import RxCocoa

extension Reactive where Base: ASButtonNode {

    public var tap: ControlEvent<Void> {
        return controlEvent(ASControlNodeEvent.touchUpInside)
    }
}
