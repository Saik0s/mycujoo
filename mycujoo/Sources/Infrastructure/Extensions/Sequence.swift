//
// Sequence.swift
//
// Created by Igor Tarasenko on 29/09/17.
//

import Foundation

extension EmptiedContainer {
    public var isNotEmpty: Bool {
        return !isEmpty
    }
}
