//
// Optional+Check.swift
// Switchback
//
// Created by Igor Tarasenko
// Copyright (c) 2017 Igor Tarasenko. All rights reserved.
//

import Foundation

public protocol EmptiedContainer {
    var isEmpty: Bool { get }
}

extension String: EmptiedContainer {
} // String already has an `isEmpty` property

extension Array: EmptiedContainer {
} // Array already has an `isEmpty` property via `CollectionType`

extension Optional where Wrapped: EmptiedContainer {
    var isNilOrEmpty: Bool {
        guard let this = self else {
            return true
        }
        return this.isEmpty
    }
}

extension Optional {
    @discardableResult
    public func ifSome(_ handler: (Wrapped) -> Void) -> Optional {
        switch self {
            case let .some(wrapped):
                handler(wrapped)
                return self
            case .none:
                return self
        }
    }

    @discardableResult
    public func ifNone(_ handler: () -> Void) -> Optional {
        switch self {
            case .some:
                return self
            case .none:
                handler()
                return self
        }
    }
}
