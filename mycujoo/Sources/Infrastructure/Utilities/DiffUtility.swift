//
// DiffUtility.swift
// Switchback
//
// Created by Igor Tarasenko.
// Copyright (c) 2017 . All rights reserved.
//

import Foundation
import IGListKit

/**
 A diffable value type that can be used in conjunction with
 `DiffUtility` to perform a diff between two result sets.
 */
public protocol Diffable: Equatable {
    /**
     Returns a key that uniquely identifies the object.

     - returns: A key that can be used to uniquely identify the object.

     - note: Two objects may share the same identifier, but are not equal.

     - warning: This value should never be mutated.
     */
    var diffIdentifier: String { get }
}

/**
 Performs a diff operation between two sets of `ItemDiffable` results.
 */
public struct DiffUtility {
    public struct DiffResult {
        public typealias Move = (from: Int, to: Int)

        public let inserts:       [Int]
        public let deletions:     [Int]
        public let updates:       [Int]
        public let moves:         [Move]
        public let oldIndexForID: (_ id: String) -> Int
        public let newIndexForID: (_ id: String) -> Int
    }

    public static func diff<T: Diffable>(originalItems: [T], newItems: [T]) -> DiffResult {

        let old: [DiffableBox<T>] = originalItems.map { item in
            DiffableBox(value: item, identifier: item.diffIdentifier as NSObjectProtocol, equal: ==)
        }
        let new: [DiffableBox<T>] = newItems.map { item in
            DiffableBox(value: item, identifier: item.diffIdentifier as NSObjectProtocol, equal: ==)
        }

        let result: ListIndexSetResult = ListDiff(oldArray: old, newArray: new, option: .equality)

        let inserts:   [Int] = Array(result.inserts)
        let deletions: [Int] = Array(result.deletes)
        let updates:   [Int] = Array(result.updates)

        let moves: [DiffResult.Move] = result.moves.map { moveIndex in
            (from: moveIndex.from, to: moveIndex.to)
        }

        let oldIndexForID: (_ id: String) -> Int = { id in
            result.oldIndex(forIdentifier: NSString(string: id))
        }

        let newIndexForID: (_ id: String) -> Int = { id in
            result.newIndex(forIdentifier: NSString(string: id))
        }

        return DiffResult(inserts: inserts,
                          deletions: deletions,
                          updates: updates,
                          moves: moves,
                          oldIndexForID: oldIndexForID,
                          newIndexForID: newIndexForID)
    }
}

public final class DiffableBox<T: Diffable>: ListDiffable {
    let value:      T
    let identifier: NSObjectProtocol
    let equal:      (T, T) -> Bool

    init(value: T, identifier: NSObjectProtocol, equal: @escaping (T, T) -> Bool) {

        self.value = value
        self.identifier = identifier
        self.equal = equal
    }

    // ListDiffable
    public func diffIdentifier() -> NSObjectProtocol {
        return identifier
    }

    public func isEqual(toDiffableObject object: ListDiffable?) -> Bool {

        if let other = object as? DiffableBox<T> {
            return equal(value, other.value)
        }
        return false
    }
}

extension String: Diffable {
    public var diffIdentifier: String {
        return self
    }
}

extension Int: Diffable {
    public var diffIdentifier: String {
        return String(self)
    }
}

extension Sequence where Iterator.Element: Diffable {
    public func diffable() -> [ListDiffable] {

        let toListDiffable: [ListDiffable] = map { listDiffable in
            DiffableBox(value: listDiffable, identifier: listDiffable.diffIdentifier as NSObjectProtocol, equal: ==)
        }
        return toListDiffable
    }
}
