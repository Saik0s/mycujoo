#!/usr/bin/env bash

set -e

[[ ! -z ${JOB_NAME} ]] && echo "Skip pre compile script on Jenkins" && exit 0

START=$(date +%s)
echo "Formatting script"

SOURCE=$([[ -z ${BASH_SOURCE[0]} ]] && echo $1 || echo ${BASH_SOURCE[0]})
while [[ -h $SOURCE ]]; do
    DIR=$(cd -P $(dirname "$SOURCE") && pwd)
    SOURCE=$(readlink "$SOURCE")
    [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
done
cd $(cd -P $(dirname "$SOURCE") && pwd)

[[ -n ${SRCROOT} ]] || SRCROOT="$PWD"

ERR_NO_SL="warn: SwiftLint is not installed. Visit http://github.com/realm/SwiftLint to learn more."
ERR_NO_SF="warn: SwiftFormat is not installed. Visit https://github.com/nicklockwood/SwiftFormat to learn more."

SWIFT_LINT="swiftlint"
which $SWIFT_LINT >/dev/null || {
    echo "${ERR_NO_SL}";
    exit 1
}

SWIFT_FORMAT="swiftformat"
which $SWIFT_FORMAT >/dev/null || {
    echo "${ERR_NO_SF}";
    exit 1
}


[[ -f ${SRCROOT}/.swiftlint.yml ]] || {
    echo ".swiftlint.yml is required";
    exit 0;
}
[[ -f ${SRCROOT}/.swiftformat.params ]] || {
    echo ".swiftformat.params is required";
    exit 0;
}
#count=0
#for file_path in $(git ls-files -om --exclude-from=.gitignore | grep ".swift$"); do
#    export SCRIPT_INPUT_FILE_$count=$file_path
#    count=$((count + 1))
#done
#for file_path in $(git diff --cached --name-only | grep ".swift$"); do
#    export SCRIPT_INPUT_FILE_$count=$file_path
#    count=$((count + 1))
#done
#
#export SCRIPT_INPUT_FILE_COUNT=$count
#
#swiftlint lint --use-script-input-files
got_error=false
echo "Auto correcting"
${SWIFT_LINT}  autocorrect --quiet --no-cache || true && got_error=true

echo "Linting"
${SWIFT_LINT}  lint --quiet --no-cache || true && got_error=true

echo "Formatting"
FORMAT_ARGS=
while read -r line; do
    [[ ${line} == --* ]] && FORMAT_ARGS+=" ${line} " || FORMAT_ARGS+="${line}"
done < <( cat  ${SRCROOT}/.swiftformat.params )
#echo "Going to use  ${SWIFT_FORMAT}  with args:  ${FORMAT_ARGS} "
$(which ${SWIFT_FORMAT}) ${SRCROOT} $(eval echo ${FORMAT_ARGS}) || true && got_error=true

END=$(date +%s)
DIFF=$[ $END - $START ]
echo "Took $DIFF seconds"

${got_error} && exit 13 || exit 0
