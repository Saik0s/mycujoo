#!/usr/bin/env bash

set -e

cd $(dirname "$(realpath "$0")")
cd ..

sourcery
buck install app -r -w -e -n "iPhone 8"

