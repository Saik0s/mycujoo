#!/usr/bin/env bash

set -e
cd "$(dirname "$(realpath "$0")")/..";

bundle update
cd Dependencies
rm -rf Rome
rm -rf build
bundle exec pod update --verbose | xcpretty

echo "" > BUCK
for filename in Rome/*.framework; do
    filename=$(basename $filename)
    out_arch=$(file "Rome/$filename/${filename%.*}" | sed -n 2p)
    if [[ "$out_arch" =~ "shared" ]]; then
        out_arch="shared"
    else
        out_arch="static"
    fi
    echo "prebuilt_apple_framework(
    name='${filename%.*}',
    framework='Rome/$filename',
    preferred_linkage='$out_arch',
    visibility=['//mycujoo/...'],
)
" >> BUCK
    echo "${out_arch} '//Dependencies:${filename%.*}',"
done